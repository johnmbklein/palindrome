import org.junit.*;
import static org.junit.Assert.*;

public class PalindromeTest {

  @Test
  public void checkPalindrome_PalindromeIsFalse_False(){
    Palindrome testPalindrome = new Palindrome();
    assertEquals(false, testPalindrome.checkPalindrome("cat"));
  }

  @Test
  public void checkPalindrome_PalindromeIsTrue(){
    Palindrome testPalindrome = new Palindrome();
    assertEquals(true, testPalindrome.checkPalindrome("pop"));
  }

}
