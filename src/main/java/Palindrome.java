import java.util.ArrayList;
import java.util.Arrays;

public class Palindrome {

  public Boolean checkPalindrome(String word) {
    ArrayList<String> myList = new ArrayList<String>(Arrays.asList(word.split("")));

    String outputString = "";

    for (Integer index = (word.length()-1); index >= 0; index--){
      myList.add(myList.get(index));
      myList.remove(index);
      outputString += myList.get(index);
    }

    System.out.println(outputString);
    if (outputString.equals(word)){
      return true;
    } else {
      return false;
    }
  }

}
